//import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { Provider } from 'react-redux';
import store from './src/redux/store';

import HomeScreen from './src/screens/HomeScreen';
import GameScreen from './src/screens/GameScreen';
import { colors } from './src/styles/base';

const Stack = createStackNavigator();

function App() {
    return (
        <Provider store={store}>
            <NavigationContainer>
                <Stack.Navigator initialRouteName="Home" screenOptions={{
                    headerTintColor: colors.violet
                }}>
                    <Stack.Screen name="Home" component={HomeScreen} options={{ title: 'Rosnou Games' }} />
                    <Stack.Screen name="Game" component={GameScreen} options={{ title: '' }} />
                </Stack.Navigator>
            </NavigationContainer>
        </Provider>
    );
}

export default App;
