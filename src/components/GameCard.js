import React from 'react';
import styled from 'styled-components/native';
import Hyperlink from 'react-native-hyperlink';
import { Linking, StyleSheet } from 'react-native';
const linkify = require('linkify-it')();

import AppTitle from '../styles/AppTitle';
import AppText from '../styles/AppText';
import AppButton from './AppButton';
import { colors } from '../styles/base';

const GameCard = ({ title, description, goToGame }) => {
    return (
        <CardContainer>
            <AppTitle size="h3" numberOfLines={3} style={{ marginBottom: 12 }}>
                {title}
            </AppTitle>
            <Hyperlink linkify={linkify} linkDefault={true} onPress={Linking.openURL} linkStyle={styles.link} style={{ marginBottom: 18 }}>
                <AppText>
                    {description}
                </AppText>
            </Hyperlink>
            <AppButton title="Играть" onPress={goToGame} accessibilityLabel="Перейти к описанию игры" />
        </CardContainer>
    );
};

const CardContainer = styled.View`
    border: 2px solid #7a7ad5;
    padding: 20px;
    border-radius: 20px;
    background-color: #a2a2ff00;
    margin-bottom: 50px;
    width: 100%;
    text-align: left;
`;

const styles = StyleSheet.create({
    link: {
        color: colors.peach,
        textDecorationLine: 'underline'
    }
});

export default GameCard;
