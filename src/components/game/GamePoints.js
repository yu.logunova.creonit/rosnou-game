import * as React from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

import AppText from '../../styles/AppText';

const GamePoints = ({ points }) => {
    return (
        <LinearGradient
            colors={['#fffe8d', '#ffc351']}
            start={[0, 1]}
            end={[1, 0]}
            style={{ borderRadius: 12 }}
        >
            <View style={styles.container}>
                <AppText style={styles.points}>
                    {points}
                </AppText>
            </View>
        </LinearGradient>
    );
};

GamePoints.propTypes = {
    points: PropTypes.oneOfType([
        PropTypes.number.isRequired,
        PropTypes.string.isRequired
    ]).isRequired
};

const styles = StyleSheet.create({
    container: {
        paddingVertical: 5,
        paddingHorizontal: 10,
        borderRadius: 50
    },
    points: {
        fontWeight: 'bold',
        fontSize: 12
    }
});

export default GamePoints;
