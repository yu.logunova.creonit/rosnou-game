import * as React from 'react';
import { useState, useEffect } from 'react';
import { View, StyleSheet, useWindowDimensions, ScrollView } from 'react-native';
import { DraxProvider, DraxList } from 'react-native-drax';
import RenderHtml from 'react-native-render-html';

import { useDispatch } from 'react-redux';
import { WEBSOCKET_ANSWER } from '../../redux/actions';

import { colors } from '../../styles/base';

const getBackgroundColor = (index) => {
    switch (index % 6) {
    case 0:
        return '#ffaaaa';
    case 1:
        return '#aaffaa';
    case 2:
        return '#aaaaff';
    case 3:
        return '#ffffaa';
    case 4:
        return '#ffaaff';
    case 5:
        return '#aaffff';
    default:
        return colors['gray-200'];
    }
};

const getItemBackgroundColor = (dictionary, item) => {
    const index = dictionary.indexOf(item);
    return {
        backgroundColor: getBackgroundColor(index)
    };
};

const QuestionCollation = ({ answers, questionId, handleIsAnswered, completed }) => {
    const dispatch = useDispatch();

    const mainParts = answers.filter(answer => answer.main);
    const contentParts = answers.filter(answer => !answer.main);

    const [result, setResult] = useState([...contentParts]);
    const handleAnswer = (newList) => {
        setResult(newList);
        const mainIds = mainParts.map(it => it.id);
        const resultIds = newList.map(it => it.id);
        const answer = mainIds.reduce((acc, rec, index) => {
            return ({ ...acc, [rec]: resultIds[index] });
        }, {});
        handleIsAnswered(true);
        dispatch({ type: WEBSOCKET_ANSWER, eventName: 'answer', payload: answer });
    };

    // изменить ширину блоков ответов, в зависимости от размера экрана
    const window = useWindowDimensions();
    let answerBlockWidth = 150;
    if (window.width <= 375) {
        answerBlockWidth = 135;
    } if (window.width <= 320) {
        answerBlockWidth = 120;
    }

    useEffect(
        () => {
            // очищать выбранный ответ, когда пришел новый вопрос
            setResult([...contentParts]);
            handleIsAnswered(false);
        },
        [questionId]
    );

    return (
        <DraxProvider>
            <View style={styles.container} pointerEvents={ completed ? 'none' : 'auto' }>
                <View>
                    <ScrollView>
                        {
                            mainParts.map(part => {
                                return (
                                    <View
                                        key={part.id}
                                        style={{ ...styles.receiver, width: answerBlockWidth, ...getItemBackgroundColor(mainParts, part) }}
                                    >
                                        <RenderHtml source={{ html: part.title.replace(/\s?<br>\s/ig, '<br>') }} />
                                    </View>
                                );
                            })
                        }
                    </ScrollView>
                </View>
                <View>
                    <DraxList
                        data={result}
                        renderItemContent={({ item }) => (
                            <View style={[...draggable, ...[{ width: answerBlockWidth }], ...[getItemBackgroundColor(result, item)]]}>
                                <RenderHtml source={{ html: item.title.replace(/\s?<br>\s/ig, '<br>') }} />
                            </View>
                        )}
                        onItemReorder={(dragData) => {
                            const newData = [...result];
                            newData.splice(dragData.toIndex, 0, newData.splice(dragData.fromIndex, 1)[0]);
                            handleAnswer(newData);
                        }}
                        keyExtractor={item => item.id.toString()}
                    />
                </View>
            </View>
        </DraxProvider>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'row'
    },
    receiver: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 150,
        minHeight: 60,
        height: 'auto',
        borderRadius: 10,
        backgroundColor: colors['gray-200'],
        margin: 10,
        paddingHorizontal: 5,
        paddingVertical: 5
    }
});

const draggable = StyleSheet.compose(
    styles.receiver,
    {
        shadowOffset: { width: 5,  height: 5  },
        shadowColor: colors['violet-200'],
        shadowOpacity: 0.5,
        shadowRadius: 3,
        elevation: 8
    }
);

export default QuestionCollation;
