import React, { useEffect, useState, useMemo } from 'react';
import PropTypes from 'prop-types';
import { Image, StyleSheet, PixelRatio } from 'react-native';
import styled from 'styled-components';
import RenderHtml from 'react-native-render-html';

import AppText from '../../styles/AppText';
import { colors } from '../../styles/base';
import QuestionSingle from './QuestionSingle';
import QuestionSpaces from './QuestionSpaces';
import QuestionCollation from './QuestionCollation';
import AppModal from '../AppModal';
import GamePoints from './GamePoints';

const fontScale = PixelRatio.getFontScale();

const GameQuestion = ({ question, sessionQuestion }) => {
    const falsyValues = ['', null, undefined, NaN, false, 0, -0];

    // определить, ответ на вопрос полностью или частично правильный
    const isCorrect = useMemo(
        () => {
            if (sessionQuestion) {
                return sessionQuestion.correct;
            }
        },
        [sessionQuestion]
    );
    const isPartiallyCorrect = useMemo(
        () => {
            if (sessionQuestion && sessionQuestion.answers) {
                return sessionQuestion.answers.some(answer => answer.correct === true);
            }

            return false;
        },
        [sessionQuestion]
    );

    // модалка результатов после окончания вопроса
    const [isResultModalOpen, setIsResultModalOpen] = useState(false);
    const handleCloseModal = () => {
        setIsResultModalOpen(false);
    };
    useEffect(
        () => {
            if (question.answered) {
                const timer = setTimeout(
                    () => setIsResultModalOpen(true),
                    200
                );

                return () => clearTimeout(timer);
            }
        },
        [question.answered]
    );
    useEffect(
        () => {
            setIsResultModalOpen(false);
        },
        [question.id]
    );

    // содержание модалки результатов
    let modalContent = <AppText> </AppText>;
    if (question.type === 'single') {
        if (question.answer_content) {
            modalContent = <RenderHtml source={{ html: question.answer_content }} />;
        }
    } else {
        modalContent = <AppText>hi</AppText>;
    }

    // набранные очки для хедера модалки
    let headerPoints;
    if (sessionQuestion && (isCorrect || isPartiallyCorrect)) {
        headerPoints = <GamePoints points={`+${sessionQuestion.points}`} />;
    } else {
        headerPoints = null;
    }

    // напоминание, что можно изменить ответ, пока не истекло время
    const [isAnswered, setIsAnswered] = useState(false);
    const handleIsAnswered = (value) => {
        setIsAnswered(value);
    };
    useEffect(
        () => {
            if (question.answered) {
                setIsAnswered(false);
            }
        },
        [question.answered]
    );

    return (
        <QuestionContainer>
            <QuestionHeader>
                {
                    falsyValues.indexOf(question.author) === -1 && (
                        <QuestionAuthorContainer>
                            <AppText fontSize={11} numberOfLines={2}>
                                <QuestionAuthor style={styles.author}>
                                    Автор вопроса: {question.author}
                                </QuestionAuthor>
                            </AppText>
                        </QuestionAuthorContainer>
                    )
                }
                {
                    question.type === 'spaces'
                        ? <AppText>Заполните пробелы в истории, выбрав из предлагаемых вариантов.</AppText>
                        : <RenderHtml source={{ html: `<span>${question.title.replace(/\s?<br>\s/ig, '<br>')}</span>` }} baseFontStyle={{ fontSize: 14 * fontScale }} />
                }
                {
                    falsyValues.indexOf(question.image) === -1 && (
                        <QuestionImageContainer>
                            <Image source={{ uri: `https://igames.team${question.image}` }} accessibilityLabel={`Картинка для вопроса ${question.title}`} style={{ width: 300, height: 300 }} />
                        </QuestionImageContainer>
                    )
                }
            </QuestionHeader>
            {
                falsyValues.indexOf(question.content) === -1 && <RenderHtml source={{ html: question.content }} baseFontStyle={{ fontSize: 13 * fontScale }} />
            }
            {
                question.type === 'single' && (
                    <QuestionSingle
                        questionId={question.id}
                        answers={question.answers}
                        handleIsAnswered={handleIsAnswered}
                        completed={question.answered}
                    />
                )
            }
            {
                question.type === 'spaces' && (
                    <QuestionSpaces
                        answers={question.answers}
                        questionId={question.id}
                        handleIsAnswered={handleIsAnswered}
                        completed={question.answered}
                    />
                )
            }
            {
                question.type === 'collation' && (
                    <QuestionCollation
                        questionId={question.id}
                        answers={question.answers}
                        handleIsAnswered={handleIsAnswered}
                        completed={question.answered}
                    />
                )
            }
            {
                isAnswered && <AppText style={styles.questionDisclaimer}>Вы можете изменить ответ, пока не истекло время.</AppText>
            }
            {
                isResultModalOpen && (
                    <AppModal
                        visible={isResultModalOpen}
                        closeModal={handleCloseModal}
                        closeButtonTitle="Понимаю"
                        modalTitle={isCorrect ? 'Правильно' : isPartiallyCorrect ? 'Частично правильно' : 'Неправильно'}
                        headerSide={headerPoints}
                    >
                        {modalContent}
                    </AppModal>
                )
            }
        </QuestionContainer>
    );
};

GameQuestion.propTypes = {
    question: PropTypes.object.isRequired,
    sessionQuestion: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.oneOf([null])
    ])
};

const styles = StyleSheet.create({
    author: {
        color: colors['gray-500']
    },
    description: {
        marginBottom: 10
    },
    questionDisclaimer: {
        marginTop: 10,
        color: colors.peach,
        textAlign: 'center'
    }
});

const QuestionContainer = styled.View`
    padding-bottom: 10px;
    flex: 1;
`;

const QuestionHeader= styled.View`
   margin-bottom: 25px;
`;

const QuestionAuthorContainer = styled.Text`
   margin-bottom: 7px;
`;

const QuestionAuthor = styled.Text`
   letter-spacing: 1px;
   font-style: italic;
`;

const QuestionImageContainer = styled.View`
    margin-top: 5px;
    align-items: center;
    justify-content: center;
`;

export default GameQuestion;
