import React from 'react';
import PropTypes from 'prop-types';
import { Platform } from 'react-native';
import styled from 'styled-components';

import AppText from '../../styles/AppText';
import CountDown from '../CountDown';
import useCountDown from '../../helpers/hooks/useCountDown';
import { colors } from '../../styles/base';
import GamePoints from './GamePoints';

const GameHeader = ({ totalQuestions, currentQuestion, timerEnd, serverDiffTime, points }) => {
    const countDownTime = useCountDown(timerEnd, serverDiffTime);

    return (
        <HeaderContainer>
            <AppText>
                <HeaderQuestion>
                    {currentQuestion}
                    <HeaderTotalQuestions style={{ color: Platform.OS === 'android' ? colors['gray-500'] : '#000000' }}>
                        /{totalQuestions}
                    </HeaderTotalQuestions>
                </HeaderQuestion>
            </AppText>
            <CountDown
                until={countDownTime}
                timeToShow={['M', 'S']}
                timeLabels={{ m: null, s: null }}
                showSeparator={true}
                digitStyle={{ backgroundColor: colors.white }}
                size={18}
            />
            <GamePoints points={points} />
        </HeaderContainer>
    );
};

GameHeader.defaultProps = {
    totalQuestions: 0,
    currentQuestion: 0
};

GameHeader.propTypes = {
    totalQuestions: PropTypes.number,
    currentQuestion: PropTypes.number,
    timerEnd: PropTypes.instanceOf(Date),
    serverDiffTime: PropTypes.number,
    points: PropTypes.string
};

const HeaderContainer = styled.View`
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    padding: 3px 15px;
    background-color: #ffffff;
    margin-bottom: auto;
`;

const HeaderQuestion = styled.Text`
    font-weight: 700;
    font-size: 13px;
`;

const HeaderTotalQuestions = styled.Text`
    opacity: 0.4;
    font-weight: 400;
`;

export default GameHeader;
