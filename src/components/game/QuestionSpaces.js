import * as React from 'react';
import { useState, useEffect } from 'react';
import { Modal, TouchableOpacity, View, StyleSheet, Image, Platform } from 'react-native';
import RenderHtml from 'react-native-render-html';
import { LinearGradient } from 'expo-linear-gradient';

import { useDispatch } from 'react-redux';
import { WEBSOCKET_ANSWER } from '../../redux/actions';

import AppText from '../../styles/AppText';
import AppButton from '../AppButton';
import { colors } from '../../styles/base';

const QuestionSpaces = ({ answers, questionId, handleIsAnswered, completed }) => {
    const dispatch = useDispatch();

    const [isSpacePressed, setIsSpacePressed] = useState(false);

    const parts = answers.filter(answer => answer.main);
    const [openModal, setOpenModal] = useState(false);
    const [currentAnswerBlockId, setCurrentAnswerBlockId] = useState('');
    const handleLayer = (id) => {
        setOpenModal(true);
        setIsSpacePressed(true);
        setCurrentAnswerBlockId(id);
    };
    const [res, setRes] = useState({});
    const handleChooseAnswer = (answerId, spaceId) => {
        const newVal = {
            [answerId]: spaceId
        };
        const result = { ...res, ...newVal };
        setRes(result);
        setOpenModal(false);
        setIsSpacePressed(false);
        handleIsAnswered(true);
        dispatch({ type: WEBSOCKET_ANSWER, eventName: 'answer', payload: result });
    };

    const mapDotsWithAnswers = (id) => {
        return answers.find(answer => answer.id === id) ? answers.find(answer => answer.id === id).title : null;
    };

    useEffect(
        () => {
            // очищать выбранный ответ, когда пришел новый вопрос
            setRes({});
            handleIsAnswered(false);
        },
        [questionId]
    );

    const onModalClose = () => {
        setOpenModal(false);
        setIsSpacePressed(false);
    };

    const handleSpacePress = (id) => {
        if (completed) return;
        handleLayer(id);
    };

    const getCorrectAnswer = (answerId) => {
        return answers.filter(answer => answer.correct && answer.answer_id === answerId)[0] || null;
    };

    const getAnswerContentForSpace = (id) => {
        let content;
        const userAnswer = mapDotsWithAnswers(+res[id]);
        const correctAnswer = getCorrectAnswer(id) && getCorrectAnswer(id).title;

        // если вопрос завершен
        if (completed) {
            content = (
                <React.Fragment>
                    <AppText style={answer.wrong}>
                        {
                            // если юзер не ответил на вопрос с переданным id
                            !userAnswer
                                ? 'без ответа '
                                // если ответ юзера не совпадает с правильным ответом
                                : userAnswer !== correctAnswer
                                    ? `${userAnswer} `
                                // если ответ юзера совпадает с правильным ответом
                                    : null
                        }
                    </AppText>
                    <AppText style={answer.correct}>
                        {correctAnswer}
                    </AppText>
                </React.Fragment>
            );
        // если юзер еще не завершил вопрос
        } else {
            // если юзер уже выбрал ответ на вопрос
            if (userAnswer) {
                content = (
                    <AppText style={styles.chosenAnswer}>
                        {userAnswer}
                    </AppText>
                );
                // если юзер еще не выбрал ответ на вопрос
            } else {
                content = (
                    <View style={[
                        styles.blankSpace,
                        {
                            borderBottomColor: isSpacePressed && id === currentAnswerBlockId ? colors.violet : colors['orange-600']
                        }
                    ]}
                    >
                        <Image style={styles.dottedImage} source={require('../../../assets/images/dots.jpeg')} />
                    </View>
                );
            }
        }

        return content;
    };

    return (
        <View style={styles.container}>
            <AppText>
                {
                    parts.map(part => {
                        return <AppText key={part.id}>
                            {
                                part.title
                                    .split(/\((\.\.\.|…)\)/)
                                    .map((it, index) => {
                                        if (it === '...' || it === '…') {
                                            return (
                                                <AppText key={`${part.id}-${index}`}>
                                                    <AppText onPress={() => handleSpacePress(part.id)}>
                                                        {getAnswerContentForSpace(part.id)}
                                                    </AppText>
                                                </AppText>
                                            );
                                        } else {
                                            return (
                                                <AppText key={it}>
                                                    <RenderHtml source={{ html: `<span>${it.replace(/<br>\s/, '<br>')}</span>` }} customWrapper={(it) => it} />
                                                </AppText>
                                            );
                                        }
                                    })
                            }
                        </AppText>;
                    })
                }
            </AppText>
            <Modal
                transparent={true}
                visible={openModal}
                onRequestClose={onModalClose}
                animationMode="fade"
            >
                <LinearGradient
                    colors={['rgba(107,108,207,.8)', 'rgba(243,154,162,.8)']}
                    start={[1, 0]}
                    end={[0, 1]}
                    style={styles.modalGradient}
                />
                <View style={styles.modalWrapper}>
                    <View style={styles.modalContainer}>
                        {
                            answers
                                .filter(answer => answer.answer_id === currentAnswerBlockId)
                                .map((answer, index, filteredAnswers) => {
                                    return (
                                        <TouchableOpacity
                                            key={answer.id}
                                            onPress={() => handleChooseAnswer(currentAnswerBlockId, answer.id)}
                                            style={index !== filteredAnswers.length - 1 ? styles.answerContainer : {}}
                                        >
                                            <AppText
                                                style={{ color: res[currentAnswerBlockId] === answer.id ? colors.violet : '#000000' }}
                                            >
                                                {answer.title}
                                            </AppText>
                                        </TouchableOpacity>
                                    );
                                })
                        }
                        <View style={styles.buttonContainer}>
                            <AppButton title="Закрыть" onPress={onModalClose} accessibilityLabel="Закрыть модальное окно с выбором ответов для пропуска" />
                        </View>
                    </View>
                </View>
            </Modal>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    blankSpace: {
        borderBottomColor: colors['orange-600'],
        borderBottomWidth: 1
    },
    dottedImage: {
        width: 70,
        height: 15,
        marginBottom: 2,
        borderTopRightRadius: 2,
        borderTopLeftRadius: 2
    },
    chosenAnswer: {
        fontWeight: 'bold',
        textDecorationLine: Platform.OS === 'ios' ? 'underline' : 'none',
        textDecorationColor: colors.violet,
        textDecorationStyle: 'solid'
    },
    modalGradient: {
        ...StyleSheet.absoluteFill
    },
    modalWrapper: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 20
    },
    modalContainer: {
        backgroundColor: colors.white,
        borderRadius: 4,
        padding: 20
    },
    answerContainer: {
        paddingBottom: 7,
        marginBottom: 7,
        borderBottomColor: colors['violet-200'],
        borderStyle: 'solid',
        borderBottomWidth: 1
    },
    buttonContainer: {
        alignItems: 'center',
        marginTop: 25
    }
});

const answer = StyleSheet.create({
    wrong: {
        color: colors.red,
        fontWeight: 'bold',
        textDecorationLine: 'line-through'
    },
    correct: {
        color: colors['green-500'],
        fontWeight: 'bold',
        textDecorationLine: 'none'
    }
});

export default QuestionSpaces;
