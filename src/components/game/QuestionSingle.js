import React, { useState, useEffect } from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import RenderHtml from 'react-native-render-html';
import { LinearGradient } from 'expo-linear-gradient';

import { useDispatch } from 'react-redux';
import { WEBSOCKET_ANSWER } from '../../redux/actions';

import { colors } from '../../styles/base';

const getAnswerGradient = (answer, chosenAnswer, isQuestionCompleted) => {
    if (answer.correct) {
        return ['#eaf3af', '#c9f4a6'];
    } else {
        if (chosenAnswer === answer.id) {
            if (isQuestionCompleted) {
                return ['#fddee1', '#ffbfc5'];
            }
            return [colors['pink-400'], colors.violet];
        }
    }

    return [colors.white, colors.white];
};

const getAnswerColor = (answer, chosenAnswer, isQuestionCompleted) => {
    if (answer.correct) {
        return '#000000';
    } else {
        if (chosenAnswer === answer.id) {
            if (isQuestionCompleted) {
                return '#000000';
            }
            return colors.white;
        }
    }

    return '#000000';
};

const QuestionSingle = ({ answers, questionId, handleIsAnswered, completed }) => {
    const dispatch = useDispatch();

    const [chosenAnswer, setChosenAnswer] = useState('');
    const handleAnswer = (id) => {
        handleIsAnswered(true);
        setChosenAnswer(id);
        dispatch({ type: WEBSOCKET_ANSWER, eventName: 'answer', payload: {
            [id.toString()]: ''
        } });
    };

    useEffect(
        () => {
            // очищать выбранный ответ, когда пришел новый вопрос
            setChosenAnswer('');
            handleIsAnswered(false);
        },
        [questionId]
    );

    return (
        <View>
            {
                answers && answers.map(answer => (
                    <TouchableOpacity
                        key={answer.id}
                        onPress={() => handleAnswer(answer.id)}
                        style={
                            chosenAnswer === answer.id
                                ? answerContainerChosen
                                : styles.answerContainer
                        }
                        disabled={completed}
                    >
                        <LinearGradient
                            colors={getAnswerGradient(answer, chosenAnswer, completed)}
                            start={[0, 1]}
                            end={[1, 0]}
                            style={styles.gradient}
                        >
                            <RenderHtml
                                source={{ html: `<span style="text-align: center;">${answer.title}</span>` }}
                                baseFontStyle={{
                                    textAlign: 'center',
                                    fontWeight: '500',
                                    color: getAnswerColor(answer, chosenAnswer, completed)
                                }}
                            />
                        </LinearGradient>
                    </TouchableOpacity>
                ))
            }
        </View>
    );
};

const styles = StyleSheet.create({
    answerContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: colors['gray-300'],
        borderWidth: 1,
        borderStyle: 'solid',
        borderRadius: 4,
        marginBottom: 5
    },
    gradient: {
        width: '100%',
        borderRadius: 4,
        paddingVertical: 10,
        paddingHorizontal: 20
    },
    answerText: {
        textAlign: 'center'
    }
});

const answerContainerChosen = StyleSheet.compose(
    styles.answerContainer,
    {
        borderColor: colors.white
    }
);

export default QuestionSingle;
