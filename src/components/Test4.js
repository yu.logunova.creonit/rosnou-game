import React, { useEffect, useRef } from 'react';
import { Text, View } from 'react-native';

import '../helpers/userAgent';
import socketIO from 'socket.io-client';
import AppButton from './AppButton';


const register = () => {
    const query = {
        contest_id: 3,
        game_id: 17
    };
};

const Test4 = () => {
    const ref = useRef();

    useEffect(
        () => {
            const socket = socketIO('https://igames.team', {
                transports: ['websocket'], // you need to explicitly tell it to use websockets
                forceNew: true,
                jsonp: false
            });
            socket.connect();
            socket.on('connect', () => {
                console.debug('SOCKET: connected to socket server');
                socket.emit('register', {
                    contest_id: 3,
                    game_id: 17
                }, (data) => console.log(data));
            });
            socket.on('error', (err) => {
                console.debug('SOCKET: errors ', err);
            });
            socket.on('connect_error', (err) => {
                console.debug('SOCKET: connect_error ---> ', err);
            });

            socket.on('welcome', (data) => {
                console.log('welcome', data);
            });

            socket.on('state', (data) => {
                console.log('SOCKET STATE', data);
            });

            ref.current = socket;

            return () => {
                socket.disconnect();
                console.debug('SOCKET: disconnected');
            };
        },
        []
    );

    const handlePress = () => {
        console.log('start');
        ref.current.emit('start', {
            contest_id: 3,
            game_id: 17
        }, (data) => console.log(data));
    };

    const handleAnswer = () => {
        ref.current.emit('answer', {
            '2565': ''
        }, (data) => console.log(data));
    };

    return (
        <View>
            <Text>test 4</Text>
            <AppButton title="start" onPress={handlePress} />
            <AppButton title="answer" onPress={handleAnswer} />
        </View>

    );
};

export default Test4;
