import React from 'react';
import { StyleSheet, TouchableOpacity, View, Text } from 'react-native';

import AppText from '../styles/AppText';
import { colors } from '../styles/base';

const AppButton = ({ title, onPress, view, icon, size, ...rest }) => {

    return (
        <TouchableOpacity
            onPress={onPress}
            style={[
                button.base,
                view === 'rounded' && button.rounded
            ]}
            accessibilityRole="button"
            {...rest}
        >
            <React.Fragment>
                {
                    icon && <View style={buttonIcon.container}>
                        {icon}
                    </View>
                }
                {
                    title && (
                        <View style={buttonText.container}>
                            <AppText fontSize={size === 'xsmall' ? 16 : 21}>
                                <Text style={buttonText.text}>
                                    {title}
                                </Text>
                            </AppText>
                        </View>
                    )
                }
            </React.Fragment>

        </TouchableOpacity>
    );
};

AppButton.defaultProps = {
    size: 'medium'
};

const button = StyleSheet.create({
    base: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors['violet-800'],
        borderRadius: 35,
        paddingHorizontal: 20,
        paddingVertical: 10,
        shadowOffset: {
            width: 5,
            height: 5
        },
        shadowColor: colors.peach,
        shadowOpacity: 0.5,
        shadowRadius: 5,
        elevation: 8
    },
    rounded: {
        borderRadius: 100,
        padding: 10,
        width: 55,
        height: 55
    },
    xsmall: {
        paddingHorizontal: 15,
        paddingVertical: 7
    }
});

const buttonText = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center'
    },
    text: {
        color: colors.white
    }
});

const buttonIcon = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center'
    }
});

export default AppButton;
