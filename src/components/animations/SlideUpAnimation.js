import React, { useRef, useEffect } from 'react';
import { View, Animated, Easing } from 'react-native';

const SlideUpAnimation = ({ children, duration, style }) => {
    const slideAnim = useRef(new Animated.Value(0)).current;

    useEffect(
        () => {
            Animated.timing(
                slideAnim,
                {
                    toValue: 1,
                    duration: duration,
                    easing: Easing.in(Easing.bounce),
                    useNativeDriver: true
                }
            ).start();
        },
        [slideAnim]
    );

    return (
        <Animated.View
            style={{
                opacity: slideAnim,
                transform: [{
                    translateY: slideAnim.interpolate({
                        inputRange: [0, 1],
                        outputRange: [100, 0]
                    })
                }],
                ...style
            }}
        >
            <View>
                {children}
            </View>
        </Animated.View>
    );
};

SlideUpAnimation.defaultProps = {
    duration: 2000
};

export default SlideUpAnimation;
