import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

const SvgArrowRight = (props) => {
    const { width, height, fill } = props;

    return (
        <Svg viewBox={`0 0 ${width} ${height}`} width={width} height={height} fill={fill}>
            <Path fill-rule="evenodd" clip-rule="evenodd" d="M11.3536 0.646484L16.7071 6.00004L11.3536 11.3536L10.6464 10.6465L14.7929 6.50004H0V5.50004H14.7929L10.6464 1.35359L11.3536 0.646484Z" />
        </Svg>
    );
};

export default SvgArrowRight;
