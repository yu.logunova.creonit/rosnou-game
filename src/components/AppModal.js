import * as React from 'react';
import PropTypes from 'prop-types';
import { Modal, View, StyleSheet } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

import { colors } from '../styles/base';
import AppButton from './AppButton';
import AppText from '../styles/AppText';

const AppModal = ({ transparent, visible, closeModal, modalTitle, headerSide, children, withCloseButton, closeButtonTitle }) => {

    return (
        <Modal
            transparent={transparent}
            visible={visible}
            onRequestClose={closeModal}
            animationMode="fade"
        >
            <LinearGradient
                colors={['rgba(107,108,207,.8)', 'rgba(243,154,162,.8)']}
                start={[1, 0]}
                end={[0, 1]}
                style={modal.gradient}
            />
            <View style={modal.wrapper}>
                <View style={modal.container}>
                    <View style={modal.header}>
                        <AppText style={modal.title}>
                            {modalTitle}
                        </AppText>
                        {
                            headerSide && <View style={modal.headerSide}>{headerSide}</View>
                        }
                    </View>
                    {children}
                    {
                        withCloseButton && (
                            <View style={modal.closeButtonContainer}>
                                <AppButton
                                    title={closeButtonTitle}
                                    onPress={closeModal}
                                    accessibilityLabel="Закрыть модальное окно" size="xsmall"
                                />
                            </View>
                        )
                    }
                </View>
            </View>
        </Modal>
    );
};

AppModal.defaultProps = {
    transparent: true,
    withCloseButton: true,
    closeButtonTitle: 'Закрыть'
};

AppModal.propTypes = {
    transparent: PropTypes.bool,
    visible: PropTypes.bool.isRequired,
    closeModal: PropTypes.func.isRequired,
    children: PropTypes.element.isRequired,
    withCloseButton: PropTypes.bool,
    closeButtonTitle: PropTypes.string,
    modalTitle: PropTypes.string,
    headerSide: PropTypes.element
};

const modal = StyleSheet.create({
    gradient: {
        ...StyleSheet.absoluteFill
    },
    wrapper: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 20
    },
    container: {
        backgroundColor: colors.white,
        borderRadius: 4,
        padding: 20
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10
    },
    title: {
        fontWeight: 'bold',
        fontSize: 18
    },
    headerSide: {
        marginLeft: 5
    },
    closeButtonContainer: {
        alignItems: 'center',
        marginTop: 25
    }
});

export default AppModal;
