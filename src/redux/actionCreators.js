import * as types from './actions';

export const getGameStart = (id) => {
    return {
        type: types.GET_GAME_START,
        id
    };
};

export const getGameSuccess = (game) => {
    return {
        type: types.GET_GAME_SUCCESS,
        game
    };
};

export const getGameError = (error) => {
    return {
        type: types.GET_GAME_ERROR,
        error
    };
};

export const initGameStart = (gameId, contestId) => {
    return {
        type: types.INIT_GAME_START,
        gameId,
        contestId
    };
};

export const initGameSuccess = (payload) => {
    return {
        type: types.INIT_GAME_SUCCESS,
        payload
    };
};

export const initGameError = (error) => {
    return {
        type: types.INIT_GAME_ERROR,
        error
    };
};
