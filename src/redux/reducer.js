import { combineReducers } from 'redux';

import getGameReducer from './reducers/getGameReducer';
import initGameReducer from './reducers/initGameReducer';

const reducers = {
    getGame: getGameReducer,
    initGame: initGameReducer
};

export const rootReducer = combineReducers(reducers);
