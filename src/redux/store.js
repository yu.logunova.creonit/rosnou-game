import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import logger from 'redux-logger';

import rootSaga from './saga';
import { rootReducer } from './reducer';


const sagaMiddleware = createSagaMiddleware();

const bindMiddleware = applyMiddleware(sagaMiddleware, logger);

const store = createStore(rootReducer, undefined, bindMiddleware);
store.sagaTask = sagaMiddleware.run(rootSaga);

export default store;
