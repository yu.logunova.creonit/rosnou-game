import { GET_GAME_START, GET_GAME_SUCCESS, GET_GAME_ERROR } from '../actions';

const INITIAL_STATE = {
    game: null,
    isLoading: false,
    error: null,
    success: false
};

export default function reducer(state = INITIAL_STATE, action) {
    switch(action.type) {
    case GET_GAME_START:
        return {
            ...state,
            isLoading: true
        };
    case GET_GAME_SUCCESS:
        return {
            ...state,
            game: action.game,
            isLoading: false,
            success: true
        };
    case GET_GAME_ERROR:
        return {
            ...state,
            isLoading: false,
            error: action.error,
            success: false
        };
    default:
        return state;
    }
}
