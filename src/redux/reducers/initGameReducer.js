import merge from 'lodash.merge';

import {
    INIT_GAME_START,
    INIT_GAME_SUCCESS,
    INIT_GAME_ERROR,
    WEBSOCKET_CONNECTED,
    WEBSOCKET_ERROR,
    WEBSOCKET_DISCONNECT,
    WEBSOCKET_RECONNECT,
    WEBSOCKET_MESSAGE,
    EXIT_GAME
} from '../actions';
import { getInitialState } from '../../helpers/game/getInitialState';
import { getInitialInstanceState } from '../../helpers/game/getInitialInstanceState';
import { getInitialSessionState } from '../../helpers/game/getInitialSessionState';

const INITIAL_STATE = {
    payload: getInitialState(),
    isLoading: false,
    error: null,
    success: false,
    isConnected: false,
    isDisconnected: false,
    isGameExited: false
};

const setState = (state, data) => {
    const input = data;
    if(data.instance){
        // можно ли не мутировать?
        state.payload.instance.question.answers.splice(0, state.payload.instance.question.answers.length);
        state.payload.instance.question.correct_sessions.splice(0, state.payload.instance.question.correct_sessions.length);
        state.payload.instance.question.correct_sessions.splice(0, state.payload.instance.question.correct_sessions.length);
        state.payload.instance.rounds.splice(0, state.payload.instance.rounds.length);
        state.payload.instance.round.sessions.splice(0, state.payload.instance.round.sessions.length);
        state.payload.instance.round.parts.splice(0, state.payload.instance.round.parts.length);
        state.payload.instance.round_part.sessions.splice(0, state.payload.instance.round_part.sessions.length);
        state.payload.instance.sessions.splice(0, state.payload.instance.sessions.length);
        state.payload.instance.completed_questions.splice(0, state.payload.instance.completed_questions.length);
        state.payload.instance.completed_groups.splice(0, state.payload.instance.completed_groups.length);
        state.payload.instance.groups.splice(0, state.payload.instance.groups.length);
        // data.instance = $.extend({}, this.getInitialInstanceState(), data.instance);
        input.instance = Object.assign({}, getInitialInstanceState(), data.instance);
    }
    if(data.session){
        state.payload.session.questions.splice(0, state.payload.session.questions.length);
        // data.session = $.extend({}, this.getInitialSessionState(), data.session);
        // input.session = Object.assign({}, state.payload.session, data.session);
        input.session = Object.assign({}, getInitialSessionState(), data.session);

        // if(window.localStorage && game.demo){
        //     localStorage.setItem(`game_${gameId}`, JSON.stringify({
        //         id: data.session.id,
        //         secret: data.session.secret
        //     }));
        // }
    }

    // console.log('SETSTATE', Object.assign({}, state.payload, input));
    // $.extend(true, this.state, data);
    const newPayload = merge({}, state.payload, input);
    return newPayload;
};

export default function reducer(state = INITIAL_STATE, action) {
    switch(action.type) {
    case INIT_GAME_START:
        return {
            ...state,
            isLoading: true
        };
    case WEBSOCKET_CONNECTED:
    case INIT_GAME_SUCCESS:
        return {
            ...state,
            isLoading: false,
            success: true,
            payload: { ...state.payload, serverDiffTime: action.data.time - Date.now() },
            isConnected: true,
            error: null
        };
    case WEBSOCKET_MESSAGE:
        return {
            ...state,
            isLoading: false,
            payload: setState(state, action.data)
        };
    case WEBSOCKET_DISCONNECT:
        return {
            ...state,
            isLoading: false,
            isConnected: false,
            success: false,
            isDisconnected: true
        };
    // если какая-то ошибка
    case WEBSOCKET_ERROR:
    case INIT_GAME_ERROR:
        return {
            ...state,
            isLoading: false,
            success: false,
            isConnected: false,
            isDisconnected: true,
            error: action.error
        };
    // вышли из игры
    case EXIT_GAME:
        return {
            ...state,
            isGameExited: true,
            isConnected: false,
            isDisconnected: true,
            isLoading: false,
            success: false,
            error: null,
            payload: getInitialState()
        };
    default:
        return state;
    }
}

// case INIT_GAME_SUCCESS:
//     return {
//         ...state,
//         payload: action.payload,
//         isLoading: false,
//         success: true
//     };
