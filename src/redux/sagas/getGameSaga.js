import axios from 'axios';
import { put, call } from 'redux-saga/effects';

import { getGameSuccess, getGameError } from '../actionCreators';

const getGameRequest = (gameId) => {
    return axios.get(`https://igames.team/api/contests/3/games/${gameId}`);
};

export default function* getGame(action) {
    try {
        const { id } = action;
        const { data: game } = yield call(getGameRequest, id);
        yield put(getGameSuccess(game));
    } catch (error) {
        console.error('saga get game error');
        yield put(getGameError(error));
    }
}
