import { eventChannel, END } from 'redux-saga';
import { call, take } from 'redux-saga/effects';

const channel = (secs) => {
    return eventChannel(emitter => {
        const iv = setInterval(
            () => {
                secs -= 1;
                if (secs > 0) {
                    emitter(secs);
                } else {
                    emitter(END);
                }
            },
            1000
        );

        return () => {
            clearInterval(iv);
        };
    });
};

export default function* count(action) {
    const { value } = action;
    const chan = yield call(channel, value);
    try {
        while(true) {
            const seconds = yield take(chan);
            console.log(seconds);
        }
    } catch (e) {
        console.log(e);
    } finally {
        console.log('end');
    }
}
