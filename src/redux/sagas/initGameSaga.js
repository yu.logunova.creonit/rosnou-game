import { eventChannel } from 'redux-saga';
import { call, take, put, apply, fork, race } from 'redux-saga/effects';

import '../../helpers/userAgent';
import io from 'socket.io-client';

import { initGameError } from '../actionCreators';
import {
    WEBSOCKET_CONNECTED,
    WEBSOCKET_ERROR,
    WEBSOCKET_DISCONNECT,
    WEBSOCKET_RECONNECT,
    WEBSOCKET_ANSWER,
    WEBSOCKET_MESSAGE,
    WEBSOCKET_SEND,
    EXIT_GAME
} from '../actions';

const createSocket = () => {
    return io('https://igames.team', {
        transports: ['websocket'], // you need to explicitly tell it to use websockets
        forceNew: true,
        jsonp: false
    });
};

const createSocketChannel = (socket, gameId, contestId) => {
    return eventChannel(emitter => {
        socket.connect();
        socket.on('connect', () => {
            socket.emit('register', {
                contest_id: contestId,
                game_id: gameId
                // session_id: 18298,
                // session_secret: '9cbe9d8c0cb59adb3c1801b1cbffa276'
            },
            (data) => {
                emitter({ type: WEBSOCKET_CONNECTED, data });
            });
        });
        socket.on('welcome', (data) => {
            //console.log('welcome', data);
        });
        socket.on('state', (data) => {
            emitter({ type: WEBSOCKET_MESSAGE, data });
        });

        // если пропадает подключение/переподключается
        socket.on('disconnect', () => {
            emitter({ type: WEBSOCKET_DISCONNECT });
        });
        // socket.on('reconnect', () => {
        //     emitter({ type: WEBSOCKET_RECONNECT });
        // });

        // если есть ошибки
        socket.on('error', (error) => {
            emitter({ type: WEBSOCKET_ERROR, error });
        });
        socket.on('connect_error', (error) => {
            emitter({ type: WEBSOCKET_ERROR, error });
        });

        return () => {
            socket.disconnect();
            emitter({ type: WEBSOCKET_DISCONNECT });
        };
    });
};

function* emitEventToSocket(socket) {
    while (true) {
        try {
            const { eventName, payload } = yield take([WEBSOCKET_SEND, WEBSOCKET_ANSWER]);
            yield apply(socket, socket.emit, [eventName, payload, (data) => console.log(data)]);
        } catch (e) {
            console.log('SAGA emitEventToSocket error', e);
        }
    }
}

export default function* initGame(action) {
    const { gameId, contestId } = action;

    const socket = yield call(createSocket);
    const socketChannel = yield call(createSocketChannel, socket, gameId, contestId);

    // while (true) {
    //     try {
    //         const { message, exit } = yield race({
    //             message: take(socketChannel),
    //             exit: take(EXIT_GAME)
    //         });
    //
    //         // если пользователь вышел из игры, закрываем канал
    //         if (exit) {
    //             socketChannel.close();
    //             break;
    //         }
    //
    //         // иначе передаем в стор полученные actions и отправляем сообщения на сервер
    //         yield put(message);
    //         yield fork(emitEventToSocket, socket);
    //     } catch (error) {
    //         yield put(initGameError(error));
    //         socketChannel.close();
    //         console.log('SAGA initGame error');
    //     }
    // }

    try {
        // инструкция Саге отправлять сообщения на сервер
        yield fork(emitEventToSocket, socket);
        while(true) {
            const { message, exit } = yield race({
                message: take(socketChannel),
                exit: take(EXIT_GAME)
            });

            // если пользователь вышел из игры, закрываем канал
            if (exit) {
                socketChannel.close();
                break;
            }

            // иначе передаем в стор полученные actions
            yield put(message);
        }
    } catch (error) {
        yield put(initGameError(error));
        socketChannel.close();
        console.log('SAGA initGame error');
    }
}

