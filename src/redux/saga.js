import { all, takeEvery } from 'redux-saga/effects';

import {
    GET_GAME_START,
    INIT_GAME_START
} from './actions';

import getGameSaga from './sagas/getGameSaga';
import testEventChannel from './sagas/testEventChannel';
import initGameSaga from './sagas/initGameSaga';

export default function* rootSaga() {
    yield all([
        takeEvery(GET_GAME_START, getGameSaga),
        takeEvery('TEST_EVENT_CHANNEL', testEventChannel),
        takeEvery(INIT_GAME_START, initGameSaga)
    ]);
}
