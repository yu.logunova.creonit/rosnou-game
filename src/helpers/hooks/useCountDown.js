import * as React from 'react';
import countdown from 'countdown';

/**
 * Возвращает в секундах время до переданной даты timerEnd (формат типа 2021-04-19T16:41:37+03:00)
 * @param timerEnd
 * @param serverDiffTime
 * @returns {number}
 */

const useCountDown = (timerEnd, serverDiffTime) => {
    const now = new Date();
    const [countDownTime, setCountDownTime] = React.useState(0);

    React.useEffect(
        () => {
            const count = countdown(now, new Date(timerEnd).getTime() - serverDiffTime, countdown.HOURS|countdown.MINUTES|countdown.SECONDS);
            let val = 0;

            if (count.hours) {
                val = count.hours * 3600 + count.minutes * 60 + count.seconds;
            } else if (count.minutes) {
                val = count.minutes * 60 + count.seconds;
            } else {
                val = count.seconds;
            }

            setCountDownTime(val);
        },
        [timerEnd, serverDiffTime]
    );

    return countDownTime;
};

useCountDown.defaultProps = {
    serverDiffTime: 0
};

export default useCountDown;
