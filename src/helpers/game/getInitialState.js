//import { useSelector } from 'react-redux';

import { getInitialInstanceState } from './getInitialInstanceState';
import { getInitialSessionState } from './getInitialSessionState';

export const getInitialState = () => {
    //const { game } = useSelector(state => state.getGame);

    return {
        instance: getInitialInstanceState(),
        initialized: false, // can delete
        connected: false,
        disconnection: false,
        answerSending: false,
        answerError: false,
        //game: game, // $.extend({}, this.game),
        //authenticated: app.authenticated,
        //member: this.member,
        session: getInitialSessionState(),
        sessions: 0,
        spectatorScoreboardVisible: false,
        //selectedLeagueSeasonId: game && game?.league_seasons.length ? game?.league_seasons[0]?.id : 0,
        selectedLeagueSeasonId: 0,
        lockJoin: false
    };
};
