export const getInitialSessionState = () => {
    return {
        id: 0,
        instance_id: null,
        league_season_id: null,
        secret: null,
        member: {
            id: 0,
            team: {
                id: 0,
                title: null
            },
            league_seasons: []
        },
        questions: [],
        complete_message: {
            title: null,
            content: null
        }
    };
};
