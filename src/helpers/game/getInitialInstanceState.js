export const getInitialInstanceState = () => {
    return {
        id: 0,
        secret: null,
        started: false,
        completed: false,
        sessions: [],
        completed_questions: [],
        completed_groups: [],
        correct: false,
        question: {
            id: 0,
            number: 0,
            title: null, // название вопроса
            content: null,
            author: null,
            answer_content: null,
            answer_image: null,
            correct_answers: null,
            image: null,
            answered: false,
            paused: false,
            extra: false,
            answers: [],
            correct_sessions: []
        },
        rounds: [],
        round: {
            id: 0,
            number: 0,
            title: null,
            sessions: [],
            parts: [],
            hard_column_number: 0,
            started: false,
            completed: false
        },
        round_part: {
            id: 0,
            number: 0,
            started: false,
            completed: false,
            sessions: []
        },
        groups: [],
        questions: 0 // сколько всего вопросов
    };
};
