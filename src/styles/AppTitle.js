import React from 'react';
import { PixelRatio } from 'react-native';
import styled from 'styled-components/native';

import AppText from './AppText';
import { colors } from './base';

const fontScale = PixelRatio.getFontScale();

const AppTitle = ({ children, size, ...rest }) => {
    return (
        <AppText {...rest}>
            <MyTitle size={size}>
                {children}
            </MyTitle>
        </AppText>
    );
};

const MyTitle = styled.Text`
    font-weight: 700;
    font-size: ${({ size }) => {
        if (size === 'h1') {
            return 27 * fontScale;
        } else if (size === 'h3') {
            return 21 * fontScale;
        } else {
            return 16 * fontScale;
        }
    }}px;
    color: ${colors.violet};
`;

export default AppTitle;
