import React from 'react';
import { PixelRatio, Text } from 'react-native';

const fontScale = PixelRatio.getFontScale();

const AppText = ({ children, fontSize, ...rest }) => {
    return (
        <Text style={{ fontSize: (+fontSize) * fontScale }} {...rest}>
            {children}
        </Text>
    );
};

AppText.defaultProps = {
    fontSize: 14
};

export default AppText;
