export const colors = {
    violet: '#7a7ad6',
    'violet-800': '#6b6ccf',
    'violet-200': '#e3e1f9',
    peach: '#f39aa2',
    red: '#CE1126',
    'green-500': '#98DA62',
    'pink-400': '#fcc5ca',
    'orange-600': '#ffc452',
    white: '#ffffff',
    'gray-500': '#b0b0b0',
    'gray-300': '#e5e5e5',
    'gray-200': '#eeeeee',
    'gray-100': '#f6f6f6'
};
