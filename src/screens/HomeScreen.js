import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, View, SafeAreaView, FlatList } from 'react-native';

import GameCard from '../components/GameCard';
import AppTitle from '../styles/AppTitle';
import AppText from '../styles/AppText';

import { GAMES_DATA } from '../../mockData/games';

function HomeScreen({ navigation }) {
    const handleNavigateToGame = (id) => navigation.navigate('Game', { id });

    return (
        <SafeAreaView style={styles.safeArea}>
            <View style={styles.container}>
                <FlatList
                    style={{ paddingHorizontal: 15 }}
                    ListHeaderComponent={
                        <View>
                            <AppTitle size="h1" style={{ marginBottom: 15, paddingTop: 20 }}>
                                Демонстрационные игры
                            </AppTitle>
                            <AppText style={{ fontSize: 12, marginBottom: 35 }}>
                                На этой странице собраны просто игры — для их прохождения не требуется регистрация, играть можно в любое время, а количество попыток не ограничено.
                                {'\n'}
                                {'\n'}
                                Попробуйте, и если понравится — задумайтесь: не этому ли вы хотите посвятить свою жизнь? ))
                            </AppText>
                        </View>
                    }
                    data={GAMES_DATA}
                    renderItem={({ item }) => (
                        <GameCard
                            title={item.title}
                            description={item.description}
                            goToGame={() => handleNavigateToGame(item.id)} />
                    )}
                    keyExtractor={item => item.id}
                />
                <StatusBar style="auto" />
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    safeArea: {
        flex: 1
    },
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center'
    }
});

export default HomeScreen;
