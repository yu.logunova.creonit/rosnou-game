import React, { useEffect, useLayoutEffect, useState, useMemo, useRef, useCallback } from 'react';
import { ImageBackground, SafeAreaView, StyleSheet, PixelRatio, ScrollView, View } from 'react-native';
import styled from 'styled-components/native';
import RenderHtml from 'react-native-render-html';
import AppLoading from 'expo-app-loading';

import { useDispatch, useSelector } from 'react-redux';
import { getGameStart, initGameStart } from '../redux/actionCreators';
import { EXIT_GAME, WEBSOCKET_SEND } from '../redux/actions';

import AppText from '../styles/AppText';
import AppButton from '../components/AppButton';
import GameHeader from '../components/game/GameHeader';
import pluralize from '../helpers/pluralize';
import CountDown from '../components/CountDown';
import { colors } from '../styles/base';
import useCountDown from '../helpers/hooks/useCountDown';
import GameQuestion from '../components/game/GameQuestion';
import SvgArrowRight from '../components/svg/ArrowRight';
import SlideUpAnimation from '../components/animations/SlideUpAnimation';

const fontScale = PixelRatio.getFontScale();

const GameScreen = ({ route }) => {
    const dispatch = useDispatch();

    const { id } = route.params;

    const {
        game,
        isLoading: isGetGameLoading,
        success: isGetGameSuccess,
        error: isGetGameError
    } = useSelector(state => state.getGame);
    const {
        isLoading: isInitGameLoading,
        isConnected,
        isDisconnected,
        payload: gameState,
        error: isInitGameError
    } = useSelector(state => state.initGame);

    const [isGameStarted, setIsGameStarted] = useState(false);

    useEffect(
        () => {
            // получаем информацию об игре (название, описание)
            dispatch(getGameStart(id));
        },
        [id]
    );

    useEffect(
        () => {
            if (isGetGameSuccess) {
                // подключаемся к сокетам
                dispatch(initGameStart(id, 3));

                // при размонтировании компонента, отключаемся от сокетов
                return () => dispatch({ type: EXIT_GAME });
            }
        },
        [isGetGameSuccess, id]
    );

    useEffect(
        () => {
            if (isDisconnected) {
                setIsGameStarted(false);
            }
        },
        [isDisconnected]
    );

    const handleStartGame = () => {
        dispatch({ type: WEBSOCKET_SEND, eventName: 'start', payload: {
            contest_id: 3,
            game_id: id
        } });
        setIsGameStarted(true);
    };

    const numberOfPoints = useMemo(() => {
        if (gameState.session.id !== 0) {
            const session = gameState?.instance?.sessions.filter(session => session.id === gameState.session.id);
            if (session.length !== 0) {
                return `${session[0].points} ${pluralize('балл|балла|баллов', session[0].points)}`;
            }
        }

        return '0 баллов';
    },
    [gameState]
    );

    const timer = useMemo(() => gameState.instance['next_update'], [gameState.instance['next_update']]);

    const countDownTime = useCountDown(timer, gameState.serverDiffTime);

    const gameScrollingContainerRef = useRef(null);
    useLayoutEffect(
        () => {
            const timer = setTimeout(
                () => {
                    if (gameScrollingContainerRef && gameScrollingContainerRef.current) {
                        // при получении нового вопроса скролл к началу
                        gameScrollingContainerRef.current.scrollTo({ y: 0, animated: true });
                    }
                },
                0
            );

            return () => clearTimeout(timer);
        },
        [gameState.instance.question.id]
    );

    // получить следующий вопрос (обновить инстанс игры)
    const getNextQuestion = useCallback(
        () => {
            dispatch({ type: WEBSOCKET_SEND, eventName: 'updateInstance', payload: {
                id: gameState.instance.id,
                secret: gameState.instance.secret
            } });
        },
        [gameState.instance.id, gameState.instance.secret]
    );

    // последний отвеченный вопрос
    const sessionQuestion = useMemo(
        () => {
            const question = gameState.session.questions.filter(question => question.id === gameState.instance.question.id);

            if (question.length) {
                return question[0];
            }

            return null;
        },
        [gameState]
    );

    let content;
    if (isGetGameLoading || isInitGameLoading) {
        content = <AppLoading />;
    } else if (isGetGameSuccess && !isGameStarted) {
        content =  (
            <GameInfo>
                <RenderHtml source={{ html: game.title }} baseFontStyle={{ fontSize: 27 * fontScale, fontWeight: 'bold', color: colors.violet }} />
                {
                    game.content ? <RenderHtml source={{ html: game.content }} /> : null
                }
                <StartGameButtonContainer>
                    <AppButton title='Начать игру' onPress={handleStartGame} accessibilityLabel="Начать игру" />
                </StartGameButtonContainer>
            </GameInfo>
        );
    } else if (isConnected && isGameStarted && !gameState.instance.started) {
        content = (
            <GameInfoWaiting>
                <GameTitleContainer>
                    <RenderHtml source={{ html: game.title }} baseFontStyle={{ fontSize: 27 * fontScale, fontWeight: 'bold', color: colors.violet }} />
                </GameTitleContainer>
                <CountDown
                    until={countDownTime}
                    timeLabels={{ d: 'дни', h: 'часы', m: 'минуты', s: 'секунды' }}
                    timeLabelStyle={{ fontSize: 14, color: colors['gray-500'], fontWeight: '500' }}
                    size={30}
                    digitTxtStyle={{ color: colors.white }}
                    digitStyle={{ backgroundColor: colors.white }}
                    withGradient={true}
                />
                <AppText style={{ fontSize: 12, marginTop: 15 }}>
                    Вы подключились к игре, ожидайте начала игры
                </AppText>
            </GameInfoWaiting>
        );
    } else if (isConnected && isGameStarted && gameState.instance.started) {
        let Wrapper;
        let wrapperProps;
        if (gameState.instance.question.type === 'collation') {
            Wrapper = View;
            wrapperProps = {
                style: { flex: 1, padding: 10, width: '100%'  }
            };
        } else {
            Wrapper = ScrollView;
            wrapperProps = {
                ref: gameScrollingContainerRef,
                contentContainerStyle: { padding: 10, width: '100%' },
                style: { flex: 1 }
            };
        }
        content = (
            <Wrapper {...wrapperProps}>
                <GameQuestion
                    question={gameState.instance.question}
                    sessionQuestion={sessionQuestion}
                />
            </Wrapper>
        );
    } else if (isGetGameError || isInitGameError) {
        content = <AppText>Произошла ошибка!</AppText>;
    }

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <ImageBackground source={require('../../assets/images/game-bg.png')} style={styles.imageBackground}>
                <GameWrapper>
                    {
                        isGameStarted && gameState.instance.started && <GameHeader
                            totalQuestions={gameState.instance.questions}
                            currentQuestion={gameState.instance.question.number}
                            timerEnd={timer}
                            serverDiffTime={gameState.serverDiffTime}
                            points={numberOfPoints}
                        />
                    }
                    <GameContainer>
                        <GameBackground>
                            {content}
                        </GameBackground>
                    </GameContainer>
                </GameWrapper>
            </ImageBackground>
            {
                isGameStarted && gameState.instance.started && <SlideUpAnimation duration={500} style={{
                    zIndex: 1,
                    position: 'absolute',
                    bottom: 15,
                    right: 15
                }}>
                    <AppButton
                        onPress={getNextQuestion}
                        icon={<SvgArrowRight width={17} height={12} fill={colors.white}/>}
                        view="rounded"
                        accessible={true}
                        accessibilityLabel="Перейти на следующий вопрос"
                    />
                </SlideUpAnimation>
            }
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    imageBackground: {
        flex: 1,
        resizeMode: 'cover',
        justifyContent: 'center'
    }
});

const GameWrapper = styled.View`
    flex: 1;
    justify-content: center;
`;

const GameContainer = styled.View`
    flex: 1;
    padding: 0 15px;
    margin: 20px 0;
`;

const GameBackground = styled.View`
    flex: 1;
    background-color: #ffffff;
    align-items: center;
    justify-content: center;
    border-radius: 4px;
    shadow-color: #f2979e;
    shadow-offset: {
        width: 6,
        height: 6,
    };
    shadow-opacity: 0.3;
    shadow-radius: 5.27px;
    elevation: 20;
`;

const GameInfo = styled.View`
    padding: 0 10px;
`;

const StartGameButtonContainer = styled.View`
    margin-top: 10px;
`;

const GameInfoWaiting = styled.View`
    flex: 1;
    align-items: center;
    justify-content: center;
`;

const GameTitleContainer = styled.View`
    margin-bottom: 15px;
`;

export default GameScreen;
