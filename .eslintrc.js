module.exports = {
    'parser': 'babel-eslint',
    'rules' : {
        'linebreak-style': ['error', 'unix'],
        'indent': ['error', 4],
        'semi': ['error', 'always'],
        'comma-dangle': ['error', 'never'],
        'comma-spacing': ['error', {
            'after': true
        }],
        'quotes': ['error', 'single'],
        'array-bracket-spacing': ['error', 'never'],
        'object-curly-spacing': ['error', 'always'],
        'no-multiple-empty-lines': ['error', { 'max': 2, 'maxBOF': 1 }]
    }
};
