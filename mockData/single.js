export const single = [
    {
        'id': 32081,
        'title': 'Этими зубами они счищали друг с друга морских паразитов '
    },
    {
        'id': 32082,
        'title': 'Они добывали еду, фильтруя воду сквозь длинные и тонкие зубы'
    },
    {
        'id': 32077,
        'title': 'Этими зубами они очищали море от отмерших водорослей'
    },
    {
        'id': 32078,
        'title': 'С помощью таких зубов они дышали, как с помощью жабр'
    },
    {
        'id': 32080,
        'title': 'Страшными длинными зубами они отпугивали ихтиозавров и мозазавров'
    },
    {
        'id': 32079,
        'title': 'Длинными тонкими зубами они протыкали панцири ракообразных '
    }
];
