export const spaces = [
    {
        'id': 32093,
        'main': true,
        'title': 'Зоологический сад был основан в Москве в 1864 году Русским императорским обществом акклиматизации животных и растений. Государство не поддерживало зоосад, поэтому он с первых дней (…) '
    },
    {
        'id': 32099,
        'main': true,
        'title': '. Всё изменилось с (…)'
    },
    {
        'id': 32105,
        'main': true,
        'title': '. В 1923 году зоосад был включён в бюджет Москвы, и началась реконструкция. <br>На переданной зоосаду в 1924 году территории возвели для коз загоны с искусственными возвышенностями, а для хищников построили просторные площадки для выгула. Для длинноногих цапель, розовых фламинго и других птиц устроили (…)'
    },
    {
        'id': 32111,
        'main': true,
        'title': '. Прежде чем открыть реконструированный зоопарк, сотрудники, по воспоминаниям Веры Чаплиной, выпустили хищников на выгулы, чтобы проверить, (…)'
    },
    {
        'id': 32117,
        'main': true,
        'title': '.<br> После реконструкции зоосад переименовали в зоопарк, потому что (…)'
    },
    {
        'id': 32123,
        'main': true,
        'title': '.'
    },
    {
        'answer_id': 32093,
        'id': 32098,
        'main': false,
        'title': 'не пускал посетителей'
    },
    {
        'answer_id': 32093,
        'id': 32094,
        'main': false,
        'title': 'оказался в тяжёлом материальном положении'
    },
    {
        'answer_id': 32111,
        'id': 32115,
        'main': false,
        'title': 'удобно ли посетителям кормить зверей'
    },
    {
        'answer_id': 32099,
        'id': 32102,
        'main': false,
        'title': 'преобразованием зоосада в зверосовхоз'
    },
    {
        'answer_id': 32111,
        'id': 32112,
        'main': false,
        'title': 'не перепрыгнут ли звери рвы с водой, заменившие заборы'
    },
    {
        'answer_id': 32117,
        'id': 32121,
        'main': false,
        'title': 'животных можно было брать домой на выходные'
    },
    {
        'answer_id': 32105,
        'id': 32110,
        'main': false,
        'title': 'городок из гигантских скворечников'
    },
    {
        'answer_id': 32117,
        'id': 32122,
        'main': false,
        'title': 'животные давали театрализованные представления'
    },
    {
        'answer_id': 32111,
        'id': 32114,
        'main': false,
        'title': 'помещаются ли звери на отведённой территории'
    },
    {
        'answer_id': 32093,
        'id': 32097,
        'main': false,
        'title': 'сделал хищников вегетарианцами, чтобы экономить на мясе'
    },
    {
        'answer_id': 32117,
        'id': 32119,
        'main': false,
        'title': 'животных можно было встретить на дорожках для посетителей'
    },
    {
        'answer_id': 32093,
        'id': 32096,
        'main': false,
        'title': 'брал кредиты у старухи-процентщицы'
    },
    {
        'answer_id': 32099,
        'id': 32100,
        'main': false,
        'title': 'национализацией'
    },
    {
        'answer_id': 32111,
        'id': 32116,
        'main': false,
        'title': 'не пугает ли рык зверей воспитанников ближайшего детского сада'
    },
    {
        'answer_id': 32099,
        'id': 32103,
        'main': false,
        'title': 'коллективизацией'
    },
    {
        'answer_id': 32105,
        'id': 32106,
        'main': false,
        'title': 'болото'
    },
    {
        'answer_id': 32099,
        'id': 32104,
        'main': false,
        'title': 'индустриализацией'
    },
    {
        'answer_id': 32093,
        'id': 32095,
        'main': false,
        'title': 'поддерживал большевиков'
    },
    {
        'answer_id': 32111,
        'id': 32113,
        'main': false,
        'title': 'хорошо ли видно зверей с дорожек'
    },
    {
        'answer_id': 32117,
        'id': 32120,
        'main': false,
        'title': 'вместе с животными там стали демонстрировать растения'
    },
    {
        'answer_id': 32099,
        'id': 32101,
        'main': false,
        'title': 'приватизацией'
    },
    {
        'answer_id': 32117,
        'id': 32118,
        'main': false,
        'title': 'животных там показывали в условиях, приближённых к естественным'
    },
    {
        'answer_id': 32105,
        'id': 32109,
        'main': false,
        'title': 'огромную голубятню'
    },
    {
        'answer_id': 32105,
        'id': 32107,
        'main': false,
        'title': 'пустыню'
    },
    {
        'answer_id': 32105,
        'id': 32108,
        'main': false,
        'title': 'оранжерею'
    }
];
